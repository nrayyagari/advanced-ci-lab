# Using dotenv to pass variables between jobs

The goal of this exercise is to show you how to [pass an environment variable from one job to another one](https://docs.gitlab.com/ee/ci/variables/#pass-an-environment-variable-to-another-job). 

We will use the image name and tag from the build job for a test job that will be run for each built container image.

## Step 01 - add script steps to create a dotenv artifact

1. Use **Build > Pipeline Editor** to edit the **_.gitlab_ci.yml_** file.  
- Add a step to the **_script_** block of the **_.build_** job to create a text file **_imagevars.env_** that contains the `$IMAGE` variable and its value:
```plaintext
    - echo "IMAGE=$IMAGE" >> imagevars.env
```

2. Save the resulting **_imagevars.env_** file as a dotenv artifact report by adding an **_artifacts_** block directly beneath the script block in your **_.build_** job:
```plaintext
  artifacts:
    reports:
      dotenv: $PROJECT_DIR/imagevars.env
```

Your **_.build_** job should now look like this:
```plaintext
.build:
  stage: build
  variables:
    IMAGE: $CI_REGISTRY_IMAGE/$PROJECT_DIR:$CI_COMMIT_SHA
  script:
    - cd $PROJECT_DIR
    - pwd
    - echo "building $PROJECT_DIR"
    - docker info
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker build -t $IMAGE .
    - docker push $IMAGE
    - echo "IMAGE=$IMAGE" >> imagevars.env
  artifacts:
    reports:
      dotenv: $PROJECT_DIR/imagevars.env
```
3. Commit your changes & then go to **Build > Pipelines** and let the pipeline finish running.  
- Use the **Download artifacts** button on the right side of the pipeline details to confirm that 2 sets of artifacts are created: **_build1:dotenv_** and **_build2:dotenv_**
- Download one of them, open it & confirm that the contents refer to the value defined by the variable IMAGE that we defined earlier.

## Step 02 - create a new job that uses the container image from the build job
1. Go back to **Build > Pipeline Editor**.  Under the **_build2_** job, add a new hidden job:

```plaintext
.get_container:
  stage: test
  script:
    - echo "The image being used is $IMAGE"
    # - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    # - docker pull $IMAGE 

```

3. Now it's time to create the specific jobs that use the variables from each build job.  Add the following jobs to the bottom of your **_.gitlab_ci.yml_** file:
```plaintext
container1_job:
  extends: .get_container
  needs: 
    - job: build1
      artifacts: true

container2_job:
  extends: .get_container
  needs: 
    - job: build2
      artifacts: true
```

- Commit your changes, then click the pipeline status icon at the top of the page to navigate to the running pipeline and view the pipeline details.  

- Click the  **_container1_job_** to view the log output.  Notice the echo command output.  

- It should output the image name and tag.  

- Go back to the pipeline details and click the **_container2_job_** job to view the logs and see which `$IMAGE` is used for that job.

### _Need additional help with this exercise? You can see the full .gitlab_ci.yml file solution for this exercise in the `2-dotenv` branch._

## Bonus
Apply the learnings from this exercise to use the [GitLab Container scanning template](https://docs.gitlab.com/ee/user/application_security/container_scanning/) to run separate container scanning jobs for each container image.

**Hints**
- Use `include` to include the standard template
- Override the default template to not run the container scanning job with the default values (set `CONTAINER_SCANNING_DISABLED: true`)
- Create a new hidden job that extends the container scanning template and re-enables it for the specific jobs that will use it ((set `CONTAINER_SCANNING_DISABLED: false`)
- Create 2 separate container scanning jobs that extends the hidden job and uses the container image from the appropriate build job

See the full solution in the **_.gitlab_ci.yml_** file in the `2-bonus-dotenv` branch.
